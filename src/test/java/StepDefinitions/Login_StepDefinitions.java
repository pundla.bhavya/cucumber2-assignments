package StepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;



	public class Login_StepDefinitions{

		static WebDriver driver=null;
		@Given("Open the website")
		public void  Open_the_browser()
		{
			driver=new ChromeDriver();
			driver.get("https://demowebshop.tricentis.com/");
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
			System.out.println("launching the  browser");
		}
		@Then("the home page of website get displayed")
		public void the_home_page_of_website_get_displayed()
		{
			System.out.println("the home page is displayed");
		}
		@When("user click on login1 button")
		public void user_click_on_login1_button()
		{
			driver.findElement(By.linkText("Log in")).click();
			
			System.out.println("user click on login button" );
		}
		@When("user enter the email {string} in email textbox")
		public void user_enter_the_email_in_email_textbox(String string)
		{
			driver.findElement(By.id("Email")).sendKeys(string);
			
			System.out.println("enter email address");
		}
		@When("user enter the password {string} in password textbox")
		public void user_enter_the_password_in_password_textbox(String string)
		{
			
			driver.findElement(By.id("Password")).sendKeys(string);
			
			System.out.println("enter password");
		}
		@Then("user click on login button")
		public void user_click_on_login_button()
		{
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			System.out.println("user click on login button" );
		}
		@Then("the home page  with logged user name should display")
		public void the_home_page_with_logged_user_name_should_display()
		{
			System.out.println("home page  with logged user name should display" );
		}
		@Then("user click on logout button")
		public void user_click_on_logout_button()
		{
			driver.findElement(By.linkText("Log out")).click();
			System.out.println("user logout");
		}
	
	}

	


